import os
import time
import pickle
from ics import Calendar
from dotenv import load_dotenv
from selenium import webdriver
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow

def main():
    #load .env or system environment variable
    try:
        load_dotenv()
        USERNAME = os.getenv('UI_SSO_USERNAME', 'default')
        PASSWORD = os.getenv('UI_SSO_PASSWORD', 'default')
    except:
        print('Missing environment variables. Configure your environment or add .env file')
        return

    if not os.path.exists('credentials.json'):
        print('Missing OAuth Client ID Credentials (credentials.json)')
        return

    #prepare download folder in the working directory
    chrome_download_path = (os.path.join(os.getcwd(),'downloads'))
    ical_file_path = os.path.join(chrome_download_path,'icalexport.ics')
    if os.path.exists(ical_file_path):
        os.remove(ical_file_path)

    #create driver and configure custom download path
    chrome_options = webdriver.ChromeOptions()
    prefs = {"download.default_directory" : chrome_download_path}
    chrome_options.add_experimental_option("prefs",prefs)
    driver = webdriver.Chrome(chrome_options=chrome_options)

    #SCELE login & redirect to calendar export page
    driver.get('https://scele.cs.ui.ac.id/calendar/export.php')
    username_input = driver.find_element_by_name('username')
    password_input = driver.find_element_by_name('password')
    username_input.send_keys(USERNAME)
    password_input.send_keys(PASSWORD)
    login_form = driver.find_element_by_id('loginbtn')
    login_form.submit()

    #specify export options to download
    try:
        radio_event = driver.find_element_by_id('id_events_exportevents_all')
        radio_time = driver.find_element_by_id('id_period_timeperiod_recentupcoming')
        export_button = driver.find_element_by_name('export')
        radio_event.click()
        radio_time.click()
        export_button.click()
    except:
        print("Wrong Username or Password")
        driver.close()
        return

    #wait for file to be downloaded
    time.sleep(4)
    driver.close()
    if not os.path.exists(ical_file_path):
        print("Download is not completed in 4 seconds, please check your internet connection")
        return

    #discover Google Service
    service = get_google_service()
    if not service: return

    #create Calendar object
    with open(ical_file_path, 'r') as file:
        export = file.read()
        c = Calendar(export)

    #call import function
    for ical_event in c.events:
        google_event = {
            'iCalUID':ical_event.uid,
            'summary':ical_event.name,
            'description':ical_event.description,
            'start':{
                'dateTime':ical_event.begin.isoformat(),
            },
            'end':{
                'dateTime':ical_event.end.isoformat(),
            },}
        service.events().import_(calendarId='primary', body=google_event).execute()
        print('Imported: ' + ical_event.name)

#Using Google OAuth Screen
def get_google_service():
    creds = None

    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', ['https://www.googleapis.com/auth/calendar',
                'https://www.googleapis.com/auth/calendar.events'])
            creds = flow.run_local_server()
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    try:
        service = build('calendar', 'v3', credentials=creds)
    except:
        print('Exception in Building Service')
        return None
    return service

if __name__ == '__main__':
    main()

